package jmdnstest.shakingearthdigital.com.jmdnstest;

import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

public class JmDnsTestActivity extends AppCompatActivity {
    static final String TAG = JmDnsTestActivity.class.getSimpleName();

    private android.net.wifi.WifiManager.MulticastLock mMulticastLock;
    private String mServiceType = "_sed_service_test._tcp.local.";
    private JmDNS mJmDNS = null;
    private ServiceListener mListener = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jm_dns_test);
    }


    @Override
    protected void onStop() {
        if (mJmDNS != null) {
            if (mListener != null) {
                mJmDNS.removeServiceListener(mServiceType, mListener);
                mListener = null;
            }
            mJmDNS.unregisterAllServices();
            try {
                mJmDNS.close();
            } catch (IOException e) {
                Log.e(TAG, "onStop", e);
            }
            mJmDNS = null;
        }
        try{
            mMulticastLock.release();
        } catch (RuntimeException e){
            e.printStackTrace();
            //we don't have a lock. just turn around and walk away.
            //walk away.
        }
        super.onStop();
    }

    /**
     * Called by Start Service button
     * Asynchronously create JmDNS object, then create custom service
     * @param view button
     */
    public void startService(final View view) {
        Log.d(TAG, "startService");
        view.setEnabled(false);

        //TODO is multicast required for service?
        android.net.wifi.WifiManager wifi = (android.net.wifi.WifiManager) getSystemService(android.content.Context.WIFI_SERVICE);
        mMulticastLock = wifi.createMulticastLock("mylockthereturn");
        mMulticastLock.setReferenceCounted(true);
        mMulticastLock.acquire();
        new AsyncTask<Void, Void, JmDNS>() {
            @Override
            protected JmDNS doInBackground(Void... params) {
                Log.d(TAG, "doInBackground");
                try {
                    return JmDNS.create();
                } catch (IOException e) {
                    Log.e(TAG, "JmDNS.create()", e);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(JmDNS jmDNS) {
                mJmDNS = jmDNS;
                ServiceInfo serviceInfo = ServiceInfo.create(mServiceType, Build.MODEL, 0, "plain test service from android");
                try {
                    mJmDNS.registerService(serviceInfo);
                    notifyUser("Service started "+Build.MODEL);
                } catch (IOException e) {
                    view.setEnabled(true);
                    Log.e(TAG, "registerService", e);
                    notifyUser("registerService() failed");
                }
            }
        }.execute();

    }

    /**
     * Called by Connect To Service button
     * Asynchronously create JmDNS object, then start listening for service
     * @param view button
     */
    public void startDiscovery(final View view) {
        Log.d(TAG, "startDiscovery");
        view.setEnabled(false);

        android.net.wifi.WifiManager wifi = (android.net.wifi.WifiManager) getSystemService(android.content.Context.WIFI_SERVICE);
        mMulticastLock = wifi.createMulticastLock("mylockthereturn");
        mMulticastLock.setReferenceCounted(true);
        mMulticastLock.acquire();
        new AsyncTask<Void, Void, JmDNS>() {
            @Override
            protected JmDNS doInBackground(Void... params) {
                Log.d(TAG, "doInBackground");
                try {
                    return JmDNS.create();
                } catch (IOException e) {
                    Log.e(TAG, "JmDNS.create()", e);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(JmDNS jmDNS) {
                mJmDNS = jmDNS;
                mJmDNS.addServiceListener(mServiceType, mListener = new ServiceListener() {

                    @Override
                    public void serviceResolved(ServiceEvent ev) {
                        notifyUser("Service resolved: " + ev.getInfo().getName() + " port:" + ev.getInfo().getPort());
                    }

                    @Override
                    public void serviceRemoved(ServiceEvent ev) {
                        notifyUser("Service removed: " + ev.getName());
                    }

                    @Override
                    public void serviceAdded(ServiceEvent event) {
                        Log.d(TAG, "serviceAdded " + event.getType() + " " + event.getName());
                        // Required to force serviceResolved to be called again (after the first search)
                        mJmDNS.requestServiceInfo(event.getType(), event.getName(), 1);
                    }
                });
            }
        }.execute();
    }


    private void notifyUser(final String msg) {
        Log.d(TAG, "notifyUser: "+msg);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView t = (TextView) findViewById(R.id.dnsTestStatusMessage);
                t.setText(msg + "\n===\n" + t.getText());
            }
        });
    }
}
